package ru.rsreu.oop_prak_1.controler;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import ru.rsreu.oop_prak_1.Array;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    Array arr;

    @FXML
    TextField field_n, field_m, field_from, field_to, field_manual, field_i, field_j;

    @FXML
    TextArea textarea_print;

    @FXML
    Label label_rezult;

    public void initialize(URL location, ResourceBundle resources) {

    }

    public void inputArrayGran(ActionEvent event) {
        arr = new Array(Integer.parseInt(field_n.getText()), Integer.parseInt(field_m.getText()));
        arr.setFrom(Integer.parseInt(field_from.getText()));
        arr.setTo(Integer.parseInt(field_to.getText()));
        arr.autoInput();
        printArray();
    }

    public void manualArrayInput(ActionEvent event) {
        int i = Integer.parseInt(field_i.getText());
        int j = Integer.parseInt(field_j.getText());
        if ((i>=0 && i<arr.getN()) || (j>=0 && j<arr.getM())) {
            arr.manualInput(i, j, Integer.parseInt(field_manual.getText()));
            printArray();
            field_manual.setText(" ");
        }
    }

    public void printArray() {
        textarea_print.setText(arr.print());
    }

    public void run(ActionEvent event) {
        label_rezult.setText("Min: " + arr.symmertyMin() + "\nMax: " + arr.symmertyMax());
    }
}
